import { businessDetails } from './reducers/business-details'

import { combineReducers } from 'redux'

const yelpApp = combineReducers({
  sample: businessDetails,
})

export default yelpApp
