import RestClient from '../RestClient'

export function reviews(state = {}, action = {}) {
  switch (action.type) {
    case 'INIT_REVIEWS': {
      return {}
    }
    case 'ADD_REVIEW': {
      let newState = Object.assign({}, state)
      newState[action.key] = action.data
      return newState
    }
    case 'SEARCH_REVIEWS': {
      new RestClient().reviews(action.businessAlias, action.dispatch)
      return state
    }
    default: {
      return {}
    }
  }
}
