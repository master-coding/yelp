import { businessDetails } from './reducers/business-details'
import { reviews } from './reducers/reviews'
import { search } from './reducers/input'

import { combineReducers } from 'redux'

const appStore = combineReducers({
  businessDetails: businessDetails,
  reviews: reviews,
  search: search,
})

export default appStore
