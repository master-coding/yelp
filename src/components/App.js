'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'
import SearchBox from './SearchBox.jsx'
import Header from './Header.jsx'
import Navigator from './Navigator.jsx'
import BusinessSummary from './BusinessSummary.jsx'
import { connect } from 'react-redux'

class App extends Component {
  constructor(props) {
    super(props)
  }
  componentWillMount() {}
  render() {
    return (
      <div className="container">
        <div className="row">
          <Header className="container-fluid" />
        </div>
        {/*<div className="row">*/}
        {/*<Navigator className="container-fluid"/>*/}
        {/*</div>*/}
        <div className="row">
          <SearchBox
            searchStatus={this.props.search}
            dispatch={this.props.dispatch}
            className="container-fluid"
          />
        </div>
        <div className="row">
          <BusinessSummary className="container-fluid" />
        </div>
      </div>
    )
  }
}
//
// App.defaultProps = {
//     "feeds": [],
//     "actionComponent": null
// };

function select(store) {
  console.log('store is called', store)
  return store
}
export default connect(select)(App)
