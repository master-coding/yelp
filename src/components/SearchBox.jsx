'use strict'

import React, {Component} from 'react'
import {render} from 'react-dom'

export default class SearchBox extends Component {
    constructor(props) {
        super(props);
        this.searchBusinesses = this.searchBusinesses.bind(this);
    }

    componentDidMount() {
        this.props.dispatch({
           type: "SPINNER_ON"
        });
        this.refs.area.value = "newyork";
        this.props.dispatch({
            type: 'SEARCH_BUSINESS',
            dispatch: this.props.dispatch,
            "search-type": "food",
            "area": "newyork"
        });
    }

    searchBusinesses(e) {
        e.preventDefault();
        this.props.dispatch({
            type: 'SPINNER_ON'
        });
        this.props.dispatch({
            type: 'INIT_REVIEWS'
        });
        this.props.dispatch({
            type: 'SEARCH_BUSINESS',
            dispatch: this.props.dispatch,
            "search-type": 'food',
            "area": this.refs.area.value.trim()
        });

    }

    render() {
        let searchComponent;
        if (this.props.searchStatus.spinner) {
            searchComponent = <i className="fa fa-spinner fa-spin" aria-hidden="false"></i>
        } else {
            searchComponent = <i className="fa fa-search"></i>
        }
        return (
            <div className="search-box">
                <input type="text" id="search-box" ref="area"/>
                <div onClick={this.searchBusinesses} className="search-icon">
                    {searchComponent}
                </div>
            </div>
        )
    }
}
