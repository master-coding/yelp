'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'
import {connect} from "react-redux";


export class BusinessComment extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {"businessProps": props.businessAlias};
    }

    componentDidMount() {
    }
    render() {
                {console.log("in component", (this.props.reviews && this.props.reviews[this.state.businessProps]  ? this.props.reviews[this.state.businessProps].reviews : []));}
        return(
            <div id="business-comment">
            {
                (this.props.reviews && this.props.reviews[this.state.businessProps]  ? this.props.reviews[this.state.businessProps].reviews : []).map((review, idx) => {
                    return (
                        <div key={`${this.state.businessProps}-${idx}`}><p>{review.text}</p> <br/></div>
                    )
                })
            }
            </div>
        )
    }
}

function select(store) {
    console.log("store = ", store);
    return store;

}

export default connect(select)(BusinessComment);

