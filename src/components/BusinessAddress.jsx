'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'

export default class BusinessAddress extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return(
            <div id="business-address">
                <p>{this.props.location.city}, {this.props.location.state}</p>
                <p>{this.props.location.address1}</p>
                <p>{this.props.location.address2}</p>
                <p>{this.props.location.address3}</p>
                <p id={'business-phone'}>{this.props.phone}</p>
            </div>
        )
    }
}
