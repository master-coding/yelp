'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'
import RestClient from '../RestClient';


export default class BusinessDetails extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return(
            <div id="business-details">
                <p id={'business-name'}>{this.props.name}</p>
                <p id={'business-review-count'}>{this.props.reviewCount} reviews</p>
                <p id={'business-rating'} className={'secondary-focus'}>{this.props.rating} rating</p>
                <p id={'price'} className={'secondary-focus'}>{this.props.price}</p>
                {/*<p>$$ . Indian, Bars</p>*/}
            </div>
        )
    }
}
