'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'

export default class Header extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return(
            <div className="header">
                <div className="logo" >
                    <i className="fa fa-cubes"/>
                    <span> Fullstack Developer</span>
                </div>
            </div>
        )
    }
}
