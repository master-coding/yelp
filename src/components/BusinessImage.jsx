'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'

export default class BusinessImage extends Component {
    constructor(props) {
        // console.log("BusinessImage.props = ", props.photos[0]);
        super(props);
    }
    render() {
        return(
            <div>
                 <img id="business-image" src={this.props.photo}/>
            </div>
    )
    }
}
