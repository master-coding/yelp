'use strict'

import React, {Component} from 'react'
import {render} from 'react-dom'
import BusinessImage from "./BusinessImage.jsx";
import BusinessDetails from "./BusinessDetails.jsx";
import BusinessAddress from "./BusinessAddress.jsx";
import BusinessComment from "./BusinessComment.jsx";
import {connect} from "react-redux";

class BusinessSummary extends Component {
    constructor(props) {
        console.log("BusinessSummary.props = ", props);
        super(props);
    }

    componentDidMount() {
    }
    render() {
        return (
            <div id="business-summary">
                <ul>
                    {(this.props.businessDetails.data ? Array.from(Array(this.props.businessDetails.data.businesses.length).keys()) : []).splice(1,2).map((image, i) => {
                        const business = this.props.businessDetails.data.businesses[i];
                        return <li key={i}>
                            <div className="boxes">
                                <div id="image-box" className="box"><BusinessImage
                                    photo={business.image_url}/></div>
                                <div id="details-box" className="box"><BusinessDetails name={business.name} price={business.price} rating={business.rating} reviewCount={business.review_count}/></div>
                                <div id="address-box" className="box"><BusinessAddress location={business.location} phone={business.display_phone}/></div>
                                <div id="comment-box" className="box"><BusinessComment businessAlias={business.alias}/></div>
                            </div>
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}

function select(store) {
    return store;

}

export default connect(select)(BusinessSummary);
