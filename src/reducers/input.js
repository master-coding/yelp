import RestClient from '../RestClient'

export function search(state = {}, action = {}) {
  switch (action.type) {
    case 'SPINNER_ON': {
      console.log('SPINNER_ON is called')
      return { spinner: true }
    }
    case 'SPINNER_OFF': {
      console.log('SPINNER_OFF is called')
      return { spinner: false }
    }
    default: {
      return state
    }
  }
}
