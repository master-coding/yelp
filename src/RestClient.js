export default class RestClient {
  static i = 0
  constructor() {
    this.authorizationKey =
      '6Y28Vsf0vuiz9akRYpaqTFOIYQS-sqlKJby7vnRhNO38WrhCwk7y1e2xzUCvtBON12k61AyePYIjS5DiZ1SbctaGzGogEnleCk_6vB_HGd1TVCWh0Xep6HRIjS1CW3Yx'
  }
  search(type, area, dispatch) {
    console.log('Search', type, area, dispatch)
    fetch(
      `http://127.0.0.1:8081/https://api.yelp.com/v3/businesses/search?term=${type}&location=${area}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.authorizationKey}`,
        },
      },
    )
      .then(
        response => {
          console.log(response.status) //=> number 100–599
          console.log(response.statusText) //=> String
          console.log(response.headers) //=> Headers
          console.log(response.url) //=> String
          response.json().then(json => {
            console.log('getBusinessDetails1', json)
            dispatch({
              type: 'SEARCH_DATA_RECEIVED',
              data: json,
            })
            dispatch({
              type: 'SPINNER_OFF',
            })

            ;(json ? Array.from(Array(json.businesses.length).keys()) : [])
              .splice(1, 2)
              .map((image, i) => {
                const business = json.businesses[i]
                this.reviews(business.alias, dispatch)
              })
          })
        },
        function(error) {
          console.log('getBusinessDetails1 error : ', error)
        },
      )
      .catch(error => {
        console.log(error)
      })
  }

  reviews(businessAlias, dispatch) {
    // if(RestClient.i > 1)
    //     return;
    // RestClient.i++;
    // console.log(i);
    console.log('BusinessAlias', businessAlias)
    fetch(
      `http://127.0.0.1:8081/https://api.yelp.com/v3/businesses/${businessAlias}/reviews`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.authorizationKey}`,
        },
      },
    ).then(
      response => {
        console.log(response.status) //=> number 100–599
        console.log(response.statusText) //=> String
        console.log(response.headers) //=> Headers
        console.log(response.url) //=> String
        response.json().then(json => {
          console.log('reviews', json)
          dispatch({
            type: 'ADD_REVIEW',
            data: json,
            key: businessAlias,
          })
        })
      },
      function(error) {
        console.log('getBusinessDetails1 error : ', error)
      },
    )
  }
}
