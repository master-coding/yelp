import RestClient from '../RestClient'
export function businessDetails(state = {}, action = {}) {
  switch (action.type) {
    case 'SEARCH_BUSINESS': {
      console.log('SEARCH_BUSINESS', action)
      let restClient = new RestClient()
      restClient.search(action['search-type'], action.area, action.dispatch)
      return {}
    }
    case 'SEARCH_DATA_RECEIVED': {
      console.log('SEARCH_DATA_RECEIVED', action)
      return action
    }
    default:
      return state
  }
}

function getBusinessDetails() {
  return {
    data: {
      search: {
        total: 3141,
        business: [
          {
            name: 'Golden Bowl Noodle House',
            phone: '+17203816398',
            display_phone: '(720) 381-6398',
            rating: 4.5,
            price: '$',
            distance: 911.9085204408923,
            review_count: 182,
            url:
              'https://www.yelp.com/biz/golden-bowl-noodle-house-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media1.fl.yelpcdn.com/bphoto/9bO-ioCXXO_bpyyH_GRn3Q/o.jpg',
            ],
            reviews: [
              {
                text:
                  'PEACEFULLY PHOTASTIC\n\nWhen I walked in I instantly felt calm because of the traditional music playing. It would calm a savage beast. There was also a good...',
              },
              {
                text:
                  'I honestly expected more from this place with all the rave reviews. Came in and got some spring rolls and bun bowls. The spring roll was basically protien...',
              },
              {
                text:
                  'Pros:\n\nExtensive menu\nVeggie broth was actually tasty\nVeggies with pho were crisp and fresh \nFair prices\nGood service\nPlentiful parking \n\nCons:\nBathrooms dirty',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1000',
                    end: '2145',
                    day: 0,
                  },
                  {
                    start: '1000',
                    end: '2145',
                    day: 1,
                  },
                  {
                    start: '1000',
                    end: '2145',
                    day: 2,
                  },
                  {
                    start: '1000',
                    end: '2145',
                    day: 3,
                  },
                  {
                    start: '1000',
                    end: '2145',
                    day: 4,
                  },
                  {
                    start: '1000',
                    end: '2145',
                    day: 5,
                  },
                  {
                    start: '1000',
                    end: '2145',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '6500 120th Ave',
              address2: 'Ste E',
              address3: '',
              formatted_address: '6500 120th Ave\nSte E\nBroomfield, CO 80020',
            },
          },
          {
            name: 'Infinitus Pie',
            phone: '+17208874588',
            display_phone: '(720) 887-4588',
            rating: 4.5,
            price: '$',
            distance: 982.7708845755177,
            review_count: 220,
            url:
              'https://www.yelp.com/biz/infinitus-pie-broomfield-4?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media1.fl.yelpcdn.com/bphoto/zg_fl2w0ptJRVU-j1lAyHA/o.jpg',
            ],
            reviews: [
              {
                text:
                  "A buy one, get one free coupon is what brought me in here, otherwise, I'm pretty sure I would've kept driving by. A friend and I stopped in for dinner and...",
              },
              {
                text:
                  'My boyfriend and I give it a 4/5 because of the process of getting your food. Allow me to explain. We went on Saturday afternoon. They had gotten a call at...',
              },
              {
                text:
                  'Unique and delicious!\nLoved this place.  Hubs and I were looking for something different and found this cool place\n..kind of hidden. And tucked...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1100',
                    end: '2100',
                    day: 0,
                  },
                  {
                    start: '1100',
                    end: '2100',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '2100',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '2100',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 4,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 5,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '145 Nickel St',
              address2: '',
              address3: null,
              formatted_address: '145 Nickel St\nBroomfield, CO 80020',
            },
          },
          {
            name: 'Azitra',
            phone: '+13034654444',
            display_phone: '(303) 465-4444',
            rating: 4.5,
            price: '$$',
            distance: 4914.526227291793,
            review_count: 381,
            url:
              'https://www.yelp.com/biz/azitra-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media1.fl.yelpcdn.com/bphoto/Cui65bntVanN6yUd2Kt-bA/o.jpg',
            ],
            reviews: [
              {
                text:
                  "Have had loads of Indian cuisine over the decades and this place stands out as one of the top two I've been to. They clearly make a more gourmet version of...",
              },
              {
                text:
                  "The food that I got from here (chicken tikka masala with naan) was actually pretty good and satisfying. I've never gotten asked about the spice level for...",
              },
              {
                text:
                  'In Fayetteville. Colorado for a wedding. I am with my Pakistani love who steers us to an Indian/Pakistani restaurant very close to our hotel. It was...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1130',
                    end: '1430',
                    day: 0,
                  },
                  {
                    start: '1700',
                    end: '2130',
                    day: 0,
                  },
                  {
                    start: '1130',
                    end: '1430',
                    day: 1,
                  },
                  {
                    start: '1700',
                    end: '2130',
                    day: 1,
                  },
                  {
                    start: '1130',
                    end: '1430',
                    day: 2,
                  },
                  {
                    start: '1700',
                    end: '2130',
                    day: 2,
                  },
                  {
                    start: '1130',
                    end: '1430',
                    day: 3,
                  },
                  {
                    start: '1700',
                    end: '2130',
                    day: 3,
                  },
                  {
                    start: '1130',
                    end: '1430',
                    day: 4,
                  },
                  {
                    start: '1700',
                    end: '2200',
                    day: 4,
                  },
                  {
                    start: '1200',
                    end: '1500',
                    day: 5,
                  },
                  {
                    start: '1700',
                    end: '2200',
                    day: 5,
                  },
                  {
                    start: '1700',
                    end: '2130',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '535 Zang St',
              address2: '',
              address3: '',
              formatted_address: '535 Zang St\nBroomfield, CO 80021',
            },
          },
          {
            name: 'Hickory & Ash',
            phone: '+17203904400',
            display_phone: '(720) 390-4400',
            rating: 4,
            price: '$$',
            distance: 1394.819820172871,
            review_count: 137,
            url:
              'https://www.yelp.com/biz/hickory-and-ash-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media4.fl.yelpcdn.com/bphoto/KB0la9oxoqAPAZ51stBKWQ/o.jpg',
            ],
            reviews: [
              {
                text:
                  'Such a lovely design and open feel! Service was very attentive and knowledgeable of the menu. \n\nWe had wedge salads and lobster bisque to start and then...',
              },
              {
                text:
                  "Okay 4.5 stars for sure! It would have been a 5 if the mash potatoes weren't sour tasting. \n\nOkay so apparently you need a reservation for this place! Even...",
              },
              {
                text:
                  'Dinner here was great.  Portions are not large, so no sharing here.  The restaurant is quaint and has limited seating so make a reservation.  \n\nThe pork...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1100',
                    end: '2200',
                    day: 0,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 4,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 5,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '8001 Arista Pl',
              address2: 'Ste 150',
              address3: '',
              formatted_address:
                '8001 Arista Pl\nSte 150\nBroomfield, CO 80021',
            },
          },
          {
            name: 'Big Dog Deli',
            phone: '+13034601000',
            display_phone: '(303) 460-1000',
            rating: 4.5,
            price: '$',
            distance: 911.7898248437511,
            review_count: 149,
            url:
              'https://www.yelp.com/biz/big-dog-deli-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media4.fl.yelpcdn.com/bphoto/xavSzXs1rl7c-fElLwcZyA/o.jpg',
            ],
            reviews: [
              {
                text:
                  'Best place for a sandwich, bread is fantastic and the family will go the extra distance to go there for their favorites. Got my attention when they won "...',
              },
              {
                text:
                  "Mastiff here is the best sandwich I've ever eaten in my life. I would eat here every day if I could",
              },
              {
                text:
                  'I got my first sandwich at this store today, the Mastiff, which was tasty, but their lack of wheat bread is baffling. The white bread is wonderful toasted,...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1000',
                    end: '1600',
                    day: 0,
                  },
                  {
                    start: '1000',
                    end: '1600',
                    day: 1,
                  },
                  {
                    start: '1000',
                    end: '1600',
                    day: 2,
                  },
                  {
                    start: '1000',
                    end: '1600',
                    day: 3,
                  },
                  {
                    start: '1000',
                    end: '1600',
                    day: 4,
                  },
                  {
                    start: '1000',
                    end: '1600',
                    day: 5,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '300 Nickel St',
              address2: 'Ste 14',
              address3: '',
              formatted_address: '300 Nickel St\nSte 14\nBroomfield, CO 80020',
            },
          },
          {
            name: 'The Burns Pub & Restaurant',
            phone: '+13034693900',
            display_phone: '(303) 469-3900',
            rating: 4.5,
            price: '$$',
            distance: 2041.82458340864,
            review_count: 203,
            url:
              'https://www.yelp.com/biz/the-burns-pub-and-restaurant-broomfield-2?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media2.fl.yelpcdn.com/bphoto/BHW3gbTw19G_tA_ya1fpJg/o.jpg',
            ],
            reviews: [
              {
                text:
                  'I absolutely loved the almost purposely kitschy yet sincerely "living room"-like atmosphere of this place. The service was very friendly and prompt. The...',
              },
              {
                text:
                  "The closest thing to a real Irish pub that I've found in our area. On Sunday nights a group gets together and brings their instruments to play Irish tunes....",
              },
              {
                text:
                  "This pub is legit! Outstanding food, outstanding service, excellent drink and the whisky collection is the best I've seen in the state of Colorado. \n\nThe...",
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1100',
                    end: '2200',
                    day: 0,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 4,
                  },
                  {
                    start: '1000',
                    end: '2200',
                    day: 5,
                  },
                  {
                    start: '1000',
                    end: '2200',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '9009 Metro Airport Ave',
              address2: '',
              address3: '',
              formatted_address: '9009 Metro Airport Ave\nBroomfield, CO 80021',
            },
          },
          {
            name: 'Waikiki Poke',
            phone: '+17205428872',
            display_phone: '(720) 542-8872',
            rating: 4.5,
            price: '$$',
            distance: 4918.914019421683,
            review_count: 34,
            url:
              'https://www.yelp.com/biz/waikiki-poke-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media2.fl.yelpcdn.com/bphoto/awWU-f1ppa3wR_h6WU2cNg/o.jpg',
            ],
            reviews: [
              {
                text:
                  'I loved it! Service is great, portions are well sized, and the ingredients are really fresh! Definitely adding to the go-to list for my coworkers and I....',
              },
              {
                text:
                  'My coworkers and I have been searching for a Poke place near our office in Westminster for months now. We were excited to see that Waikiki Poke had...',
              },
              {
                text:
                  'If I lived near here, I would be a regular but it was nice to have a chance to stop in recently when I happened to be quite near and hungry. It was a very...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1100',
                    end: '2000',
                    day: 0,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 4,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 5,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '535 Zang St',
              address2: '',
              address3: '',
              formatted_address: '535 Zang St\nBroomfield, CO 80021',
            },
          },
          {
            name: 'North Side Tavern',
            phone: '+13034668643',
            display_phone: '(303) 466-8643',
            rating: 4,
            price: '$$',
            distance: 4024.612555814001,
            review_count: 203,
            url:
              'https://www.yelp.com/biz/north-side-tavern-broomfield-3?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media4.fl.yelpcdn.com/bphoto/-g0YsDjnISdOaud8F19u5Q/o.jpg',
            ],
            reviews: [
              {
                text:
                  "So many gluten free options!! The fried pickles brought tears to my eyes because they were ACTUALLY crunchy and not mushy. My fellow Celiacs know what I'm...",
              },
              {
                text:
                  'If I could give this place zero stars, I would.\n\nAfter having read their menu online, I was really looking forward to a great meal.\n\nUnfortunately, the...',
              },
              {
                text:
                  "This review needs a disclaimer.\n\n**We eat here all of the time. Weekly. I'm biased. You've been warned.**\n\nMy husband and I have recently developed the...",
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1100',
                    end: '2100',
                    day: 0,
                  },
                  {
                    start: '1100',
                    end: '2300',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '2300',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '2300',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '0000',
                    day: 4,
                  },
                  {
                    start: '1000',
                    end: '0000',
                    day: 5,
                  },
                  {
                    start: '1000',
                    end: '2100',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '12708 Lowell Blvd',
              address2: null,
              address3: '',
              formatted_address: '12708 Lowell Blvd\nBroomfield, CO 80020',
            },
          },
          {
            name: 'Kachina Southwestern Grill - Westminster',
            phone: '+13034105813',
            display_phone: '(303) 410-5813',
            rating: 4,
            price: '$$',
            distance: 3072.006221781952,
            review_count: 583,
            url:
              'https://www.yelp.com/biz/kachina-southwestern-grill-westminster-westminster?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media1.fl.yelpcdn.com/bphoto/uXzIgAjF60nCCOExPxBghg/o.jpg',
            ],
            reviews: [
              {
                text:
                  'Just stopped in for a drink while checking in to the Westin. Had a specialty drink called the porch song, tequila and grapefruit juice   Wanted to try...',
              },
              {
                text:
                  "I've been in a few times. The beignets are sooo good. The spicey caramel sauce is just perfect and I always try to get some to-go. \nI like the tacos, but...",
              },
              {
                text:
                  'We went here on a whim, wanting to go to Snooze across the street and it being super packed at 8am on a Sunday. \n\nI have to say this place is pretty...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '0630',
                    end: '2300',
                    day: 0,
                  },
                  {
                    start: '0630',
                    end: '2300',
                    day: 1,
                  },
                  {
                    start: '0630',
                    end: '2300',
                    day: 2,
                  },
                  {
                    start: '0630',
                    end: '2300',
                    day: 3,
                  },
                  {
                    start: '0630',
                    end: '2300',
                    day: 4,
                  },
                  {
                    start: '0630',
                    end: '2300',
                    day: 5,
                  },
                  {
                    start: '0630',
                    end: '2300',
                    day: 6,
                  },
                ],
                is_open_now: true,
              },
            ],
            location: {
              city: 'Westminster',
              state: 'CO',
              address1: '10600 Westminster Blvd',
              address2: '',
              address3: '',
              formatted_address:
                '10600 Westminster Blvd\nWestminster, CO 80020',
            },
          },
          {
            name: 'Loftea Cafe',
            phone: '+17204450552',
            display_phone: '(720) 445-0552',
            rating: 4.5,
            price: '$',
            distance: 1484.3807097554727,
            review_count: 143,
            url:
              'https://www.yelp.com/biz/loftea-cafe-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media2.fl.yelpcdn.com/bphoto/xLw95KK_G4jQv8I6F5byGQ/o.jpg',
            ],
            reviews: [
              {
                text:
                  'What an amazing little find!  Dropped Hubby off for a Meeting.  Was looking for a perk up and to kill a little time.... What I did was find an absolute GEM!...',
              },
              {
                text:
                  'I love this place. Baristas are super friendly and accommodating, reasonable prices, vegan options, really homey feel to the cafe. Sometimes when it gets...',
              },
              {
                text:
                  'Nice, casual place to grab a good beverage!  Clean and friendly.  A few cool specialty drinks like their Golden Latte and their 24hr cold brew (which is...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '0630',
                    end: '1800',
                    day: 0,
                  },
                  {
                    start: '0630',
                    end: '1800',
                    day: 1,
                  },
                  {
                    start: '0630',
                    end: '1800',
                    day: 2,
                  },
                  {
                    start: '0630',
                    end: '1800',
                    day: 3,
                  },
                  {
                    start: '0630',
                    end: '1800',
                    day: 4,
                  },
                  {
                    start: '0700',
                    end: '1700',
                    day: 5,
                  },
                  {
                    start: '0800',
                    end: '1400',
                    day: 6,
                  },
                ],
                is_open_now: true,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '11542 Colony Row',
              address2: '',
              address3: '',
              formatted_address: '11542 Colony Row\nBroomfield, CO 80021',
            },
          },
          {
            name: 'Blue Sky Bistro',
            phone: '+17206285213',
            display_phone: '(720) 628-5213',
            rating: 4.5,
            price: '$',
            distance: 2981.871744868223,
            review_count: 53,
            url:
              'https://www.yelp.com/biz/blue-sky-bistro-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media3.fl.yelpcdn.com/bphoto/k86VlY6y3cp2CWQPTI4eyQ/o.jpg',
            ],
            reviews: [
              {
                text:
                  'Consistently outstanding quality food and service. You will go back. One of us says the "best BBQ" ever.  That\'s "E V E R".  No small thing.',
              },
              {
                text:
                  'Thank you yelp, for showing me this gem. I went here while in town at a work conference at the Omni. Needless to say, I was quite surprised when we got to...',
              },
              {
                text:
                  'I work on Airport Rd in Broomfield, and for a place in walking distance that has quality food with value, Blue Sky Bistro is a hit. They do a tasty and...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '0700',
                    end: '1400',
                    day: 0,
                  },
                  {
                    start: '0700',
                    end: '1400',
                    day: 1,
                  },
                  {
                    start: '0700',
                    end: '1400',
                    day: 2,
                  },
                  {
                    start: '0700',
                    end: '1400',
                    day: 3,
                  },
                  {
                    start: '0700',
                    end: '1400',
                    day: 4,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '11755 Airport Way',
              address2: '',
              address3: '',
              formatted_address: '11755 Airport Way\nBroomfield, CO 80021',
            },
          },
          {
            name: 'ProsperOats - Broomfield',
            phone: '+17202552892',
            display_phone: '(720) 255-2892',
            rating: 4.5,
            price: '$',
            distance: 1394.819820172871,
            review_count: 18,
            url:
              'https://www.yelp.com/biz/prosperoats-broomfield-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media1.fl.yelpcdn.com/bphoto/h5M7yfQ5ACEWUQEV3QBYuw/o.jpg',
            ],
            reviews: [
              {
                text:
                  'A great and healthy breakfast option! I had the 12 oz steel cut oats with my choice of three toppings (flour dusted dates, granola, and sliced almonds) with...',
              },
              {
                text:
                  "This place can't be any more broomfield.  I walk in here and I feel like I am walking from the beach into a beach house, its a nice feel.  I came here for...",
              },
              {
                text:
                  'I was a little overwhelmed on what to order but the staff was very helpful.  I decided on the acai bowl topped with berries, bananas, and granola.  They...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '0630',
                    end: '1500',
                    day: 0,
                  },
                  {
                    start: '0630',
                    end: '1500',
                    day: 1,
                  },
                  {
                    start: '0630',
                    end: '1500',
                    day: 2,
                  },
                  {
                    start: '0630',
                    end: '1500',
                    day: 3,
                  },
                  {
                    start: '0630',
                    end: '1500',
                    day: 4,
                  },
                  {
                    start: '0700',
                    end: '1400',
                    day: 5,
                  },
                  {
                    start: '0800',
                    end: '1300',
                    day: 6,
                  },
                ],
                is_open_now: true,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '8001 Arista Pl',
              address2: 'Ste 125',
              address3: null,
              formatted_address:
                '8001 Arista Pl\nSte 125\nBroomfield, CO 80021',
            },
          },
          {
            name: 'La Casa Del Burrito',
            phone: '+13034653567',
            display_phone: '(303) 465-3567',
            rating: 4.5,
            price: '$',
            distance: 651.1367693182452,
            review_count: 102,
            url:
              'https://www.yelp.com/biz/la-casa-del-burrito-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media4.fl.yelpcdn.com/bphoto/G61UObJ1KRQHi-t7-awwXw/o.jpg',
            ],
            reviews: [
              {
                text:
                  "Dios mio, these chilaquiles are so good. I've been here five times now and I still haven't had anything else, because I can't pass up the chance to eat...",
              },
              {
                text:
                  "Don't be fooled by the name!! While LCDB does have a good selection of burritos, both regular and breakfast, the menu extends far beyond burritos. In fact,...",
              },
              {
                text:
                  'Breakfast burritos are great but huge! Shared one with my boyfriend....The extra one will be at.least two more meals!',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '0600',
                    end: '1500',
                    day: 0,
                  },
                  {
                    start: '0600',
                    end: '1500',
                    day: 1,
                  },
                  {
                    start: '0600',
                    end: '1600',
                    day: 2,
                  },
                  {
                    start: '0600',
                    end: '1600',
                    day: 3,
                  },
                  {
                    start: '0600',
                    end: '1600',
                    day: 4,
                  },
                  {
                    start: '0700',
                    end: '1600',
                    day: 5,
                  },
                  {
                    start: '0800',
                    end: '1400',
                    day: 6,
                  },
                ],
                is_open_now: true,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '1384 US 287',
              address2: '',
              address3: '',
              formatted_address: '1384 US 287\nBroomfield, CO 80020',
            },
          },
          {
            name: "Bad Daddy's Burger Bar",
            phone: '+13034382143',
            display_phone: '(303) 438-2143',
            rating: 4,
            price: '$$',
            distance: 4866.876821045035,
            review_count: 72,
            url:
              'https://www.yelp.com/biz/bad-daddys-burger-bar-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media4.fl.yelpcdn.com/bphoto/udyugXRDMb1PWxSUW08NrQ/o.jpg',
            ],
            reviews: [
              {
                text:
                  'I seriously love coming here! I wish there was one closer to me.\n\nI am addicted to doing the CYO Salad which is "create your own". I enjoy getting to pick...',
              },
              {
                text:
                  "The food here is very good but the ambiance wasn't my favorite. We came in for a date night and couldn't hear each other. The music was tinny and my husband...",
              },
              {
                text:
                  'Introduction: Opening their first location in Charlotte North Carolina in 2007, only five years later they had 5 more spots open, they now have several...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1100',
                    end: '2200',
                    day: 0,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 4,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 5,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '1 W Flatiron Crossing Dr',
              address2: 'Ste 2156',
              address3: null,
              formatted_address:
                '1 W Flatiron Crossing Dr\nSte 2156\nBroomfield, CO 80021',
            },
          },
          {
            name: 'GQue BBQ',
            phone: '+13033799205',
            display_phone: '(303) 379-9205',
            rating: 4.5,
            price: '$$',
            distance: 2328.592123689051,
            review_count: 393,
            url:
              'https://www.yelp.com/biz/gque-bbq-westminster?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media2.fl.yelpcdn.com/bphoto/Yc_SeezSANZeDYa0G3wNUw/o.jpg',
            ],
            reviews: [
              {
                text:
                  'Delicious BBQ in Westminster.\n\nChoose your type of meat and whether you want it on a sandwich or plain. \n\nThe brisket is soft tender and full of smokey...',
              },
              {
                text:
                  "This place is awesome, this BBQ recipe didn't win awards for nothing!! They over pack the orders which is amazing because they cost slightly more than the...",
              },
              {
                text:
                  "Walked in at 3pm or 4pm and was the only customer, so they couldn't have provided bad service unless they tried.\n\nI ordered pulled pork, brisket, and two...",
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1100',
                    end: '2000',
                    day: 0,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '2030',
                    day: 4,
                  },
                  {
                    start: '1100',
                    end: '2030',
                    day: 5,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Westminster',
              state: 'CO',
              address1: '5160 W 120th Ave',
              address2: 'Unit K',
              address3: '',
              formatted_address:
                '5160 W 120th Ave\nUnit K\nWestminster, CO 80020',
            },
          },
          {
            name: 'The Post Brewing Co. Lafayette',
            phone: '+13035932066',
            display_phone: '(303) 593-2066',
            rating: 4,
            price: '$$',
            distance: 8743.99617060289,
            review_count: 477,
            url:
              'https://www.yelp.com/biz/the-post-brewing-co-lafayette-lafayette?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media2.fl.yelpcdn.com/bphoto/oHmgpkB9ojxG-25CQcwHDg/o.jpg',
            ],
            reviews: [
              {
                text:
                  "This was our first time here. It was outstanding. Taste very very close to my late Grandma Lou's pan fried chicken.  Parking was OK at 3 o'clock on a...",
              },
              {
                text:
                  'This is the first time for us at this location. I thought the chicken was good but not outstanding like in my previous experience. The biscuits were okay...',
              },
              {
                text:
                  'Came on Sunday after a hiatus from The Post. I was pleasantly surprised to find the menu adjusted to feature a much better value to the customer. The beer...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1600',
                    end: '2100',
                    day: 0,
                  },
                  {
                    start: '1600',
                    end: '2100',
                    day: 1,
                  },
                  {
                    start: '1600',
                    end: '2100',
                    day: 2,
                  },
                  {
                    start: '1600',
                    end: '2100',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '2200',
                    day: 4,
                  },
                  {
                    start: '1000',
                    end: '2200',
                    day: 5,
                  },
                  {
                    start: '1000',
                    end: '2100',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Lafayette',
              state: 'CO',
              address1: '105 W Emma St',
              address2: '',
              address3: '',
              formatted_address: '105 W Emma St\nLafayette, CO 80026',
            },
          },
          {
            name: 'Scalzotto Italian Restaurant',
            phone: '+13034656196',
            display_phone: '(303) 465-6196',
            rating: 4.5,
            price: '$$',
            distance: 1003.3540057823283,
            review_count: 218,
            url:
              'https://www.yelp.com/biz/scalzotto-italian-restaurant-broomfield?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media1.fl.yelpcdn.com/bphoto/5bcgPL5p0_TiSFw2-2irlA/o.jpg',
            ],
            reviews: [
              {
                text:
                  'My wife and I came in for the first time yesterday. I am very critical about Italian cuisine and find most of the local Italian food appalling. I was unsure...',
              },
              {
                text:
                  'You have to try the Zuppa Genovese for two solid reasons.\n\nFirst, it just sounds so cool rolling off your tongue, your first thought...its one if Super...',
              },
              {
                text:
                  'Very disappointed! The decor and the ambiance is amazing. But the service was terrible. My husband order two glasses of wine. A Pinot Grigio and a merlot....',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1700',
                    end: '2100',
                    day: 0,
                  },
                  {
                    start: '1100',
                    end: '1400',
                    day: 1,
                  },
                  {
                    start: '1700',
                    end: '2100',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '1400',
                    day: 2,
                  },
                  {
                    start: '1700',
                    end: '2100',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '1400',
                    day: 3,
                  },
                  {
                    start: '1700',
                    end: '2100',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '1400',
                    day: 4,
                  },
                  {
                    start: '1700',
                    end: '2100',
                    day: 4,
                  },
                  {
                    start: '1700',
                    end: '2100',
                    day: 5,
                  },
                  {
                    start: '1700',
                    end: '2100',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '88 Lamar St',
              address2: 'Ste 110',
              address3: '',
              formatted_address: '88 Lamar St\nSte 110\nBroomfield, CO 80020',
            },
          },
          {
            name: 'The Egg & I',
            phone: '+13034392202',
            display_phone: '(303) 439-2202',
            rating: 4.5,
            price: '$$',
            distance: 1623.541615050599,
            review_count: 116,
            url:
              'https://www.yelp.com/biz/the-egg-and-i-broomfield-2?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media4.fl.yelpcdn.com/bphoto/6L2NDzEFXV9bC6wHEib_fw/o.jpg',
            ],
            reviews: [
              {
                text:
                  'Excellent brunch spot! Everyone who works here is wonderful!\nHuge menu with lots of great choices!\nWe come here pretty regularly and always have good meals....',
              },
              {
                text:
                  'Came here as a suggestion from our Air BnB hosts and were happy they suggested it. The place was very clean and the staff was very welcoming as soon as we...',
              },
              {
                text:
                  "First off, i'm a huge fan of their menu.  As a breakfast/brunch place they have just about anything I would ever crave.   I expected this place to have a...",
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '0600',
                    end: '1400',
                    day: 0,
                  },
                  {
                    start: '0600',
                    end: '1400',
                    day: 1,
                  },
                  {
                    start: '0600',
                    end: '1400',
                    day: 2,
                  },
                  {
                    start: '0600',
                    end: '1400',
                    day: 3,
                  },
                  {
                    start: '0600',
                    end: '1400',
                    day: 4,
                  },
                  {
                    start: '0700',
                    end: '1430',
                    day: 5,
                  },
                  {
                    start: '0700',
                    end: '1430',
                    day: 6,
                  },
                ],
                is_open_now: true,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '799 US Hwy 287',
              address2: '',
              address3: '',
              formatted_address: '799 US Hwy 287\nBroomfield, CO 80020',
            },
          },
          {
            name: "Papa Frank's",
            phone: '+13034691401',
            display_phone: '(303) 469-1401',
            rating: 4,
            price: '$$',
            distance: 948.9597757664768,
            review_count: 30,
            url:
              'https://www.yelp.com/biz/papa-franks-broomfield-2?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media2.fl.yelpcdn.com/bphoto/4DI65sS4Be1I4-NEpGnuPQ/o.jpg',
            ],
            reviews: [
              {
                text:
                  "I genuinely wish I lived closer to Papa Frank's. The BEST homemade noodles I have ever had. The sauce has the perfect amount of spice, and it's so savory......",
              },
              {
                text:
                  'We used to have Papa Franks close to home here in Frederick, but they closed down. We decided to drive to this location and dine in for their Sunday special...',
              },
              {
                text:
                  "Mama Mia.... walking in the smells of home made sauce permeates the air.... papa franks is definitely a favorite.    You can't go wrong with all home made...",
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '1100',
                    end: '2000',
                    day: 1,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 2,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 3,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 4,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 5,
                  },
                  {
                    start: '1100',
                    end: '2000',
                    day: 6,
                  },
                ],
                is_open_now: false,
              },
            ],
            location: {
              city: 'Broomfield',
              state: 'CO',
              address1: '300 Nickel St',
              address2: 'Unit 21',
              address3: '',
              formatted_address: '300 Nickel St\nUnit 21\nBroomfield, CO 80020',
            },
          },
          {
            name: 'Snooze An A M Eatery',
            phone: '+13034819925',
            display_phone: '(303) 481-9925',
            rating: 4,
            price: '$$',
            distance: 3223.5741448351787,
            review_count: 186,
            url:
              'https://www.yelp.com/biz/snooze-an-a-m-eatery-westminster?adjust_creative=ywn5q28flcLlgwdHKDoOLA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=ywn5q28flcLlgwdHKDoOLA',
            photos: [
              'https://s3-media4.fl.yelpcdn.com/bphoto/BTC-PFbBXUiiVL4FL5kalQ/o.jpg',
            ],
            reviews: [
              {
                text:
                  'Loved the healthy breakfast options. \nFast service. Friendly servers. \nWould highly recommend. \nGet there early. It feels up quickly.',
              },
              {
                text:
                  "The wait is at least 90 minutes every single time I have tried to eat here.  I've never made to actually eat here.  \nThe first time, the wait extended from...",
              },
              {
                text:
                  'so I have been to the snooze in Fort Collins and WOW it was AMAZING......I wanted my son to try snooze for the first time so we decided to go to the...',
              },
            ],
            hours: [
              {
                open: [
                  {
                    start: '0630',
                    end: '1430',
                    day: 0,
                  },
                  {
                    start: '0630',
                    end: '1430',
                    day: 1,
                  },
                  {
                    start: '0630',
                    end: '1430',
                    day: 2,
                  },
                  {
                    start: '0630',
                    end: '1430',
                    day: 3,
                  },
                  {
                    start: '0630',
                    end: '1430',
                    day: 4,
                  },
                  {
                    start: '0630',
                    end: '1430',
                    day: 5,
                  },
                  {
                    start: '0630',
                    end: '1430',
                    day: 6,
                  },
                ],
                is_open_now: true,
              },
            ],
            location: {
              city: 'Westminster',
              state: 'CO',
              address1: '6315 W 104th Ave',
              address2: 'Ste 100',
              address3: null,
              formatted_address:
                '6315 W 104th Ave\nSte 100\nWestminster, CO 80020',
            },
          },
        ],
      },
    },
  }
}
